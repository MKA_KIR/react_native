import React from "react";
import {StyleSheet,TextInput, View} from 'react-native';

export default function Input({placeholder, keyboardType, value}) {

    return (
        <View>
            <TextInput
                style={styles.input}
                placeholder={placeholder}
                keyboardType={keyboardType}
                value={value}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    input:{
        height: 40,
        margin: 12,
        padding: 10,
        color: "#000",
        borderColor: '#fff',
        borderRadius: 25,
        backgroundColor: '#fff',
        textAlign:'center',
        borderStyle:'solid',
        borderWidth: 1,
        display: 'inline-block',
        paddingTop: 10,
        paddingBottom: 10,
        marginRight: 20,
    }
});
