import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from "../shared/Button/Button";

import Input from "../shared/Input/Input";
import { AntDesign } from '@expo/vector-icons';


export default function Login() {
    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.text}>Login</Text>
                <Input placeholder='Email'/>
                <Input placeholder='Password'/>
                <Button text='Login'/>
                <Text style={styles.text}>Forget your password?</Text> // Это должна быть ссылка
                <Text style={styles.text}>Dont have an account? <Text>Sign up</Text></Text> // Sign up Это должна быть ссылка
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: '0 0 100%',
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    },
    text:{

    }
});
