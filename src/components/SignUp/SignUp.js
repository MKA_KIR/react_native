import React, {useState} from 'react';
import {StyleSheet, Text, View, CheckBox} from 'react-native';


import Button from "../shared/Button/Button";
import Input from "../shared/Input/Input";
import {AntDesign} from '@expo/vector-icons';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function SignUp() {
    const [isSelected, setSelection] = useState(false);
    return (
        <View style={styles.container}>
            <Text>Sign up</Text>
            <Input placeholder='Your name'/>
            <View style={styles.inputIconContainer}>
                <AntDesign name="linkedin-square" style={styles.inputIcon} />
                    <Input placeholder='Email'/>
            </View>
            <Input placeholder='Phone'/>
            <Input placeholder='Password'/>
            <View style={styles.checkboxContainer}>
                <CheckBox
                    value={isSelected}
                    onValueChange={setSelection}
                    style={styles.checkbox}
                />
                <Text>Yes ! Agree all Tearms & Condition</Text>
            </View>
            <Button text='Sign up'/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: '0 0 100%',
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    },
    inputIconContainer:{
        flex: 1,
        alignItems: 'center',
        backgroundColor:'white',
    },
    inputIcon:{
        backgroundColor: 'red',
        width:45,
        height:45,
        position:'absolute',
        left: 30,
        top:2
    },
    checkboxContainer: {},
    checkbox: {
        alignSelf: "center",
    }

});
