import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Start from "./src/components/start/start";
import Login from "./src/components/Login/Login";
import SignUp from "./src/components/SignUp/SignUp";

export default function App() {
  return (
    <View style={styles.container}>
      {/*<Start />*/}
      {/*  <Login/>*/}
        <SignUp/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
      height: '100%',
  },
});
